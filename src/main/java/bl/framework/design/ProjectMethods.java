package bl.framework.design;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import bl.framework.api.SeleniumBase;
import utils.ReadExcel;

public class ProjectMethods extends SeleniumBase {
	
	public String testCaseName, testDesc,author, category, dataSheetName;
	
	
	@Parameters({"url","username","password"})
	@BeforeMethod
	public void login(String url,String uname, String pwd)
	{
		
		//Login
				startApp("chrome", url);
				WebElement eleUsername = locateElement("id", "username");
				clearAndType(eleUsername, uname); 
				WebElement elePassword = locateElement("id", "password");
				clearAndType(elePassword, pwd); 
				WebElement eleLogin = locateElement("class", "decorativeSubmit");
				click(eleLogin); 

		
	}
	
	@BeforeSuite
	 public void beforeSuite()

	 {
		 startReport();
	 }
	@AfterSuite
	public void afterSuite()
	{
		endReport();
	}
	
	@BeforeClass
	public void beforeClass()
	{
		initializeTest(testCaseName, testDesc, author, category);
	}
	@AfterMethod
	public void closeApp()
	{
		close();
	}
	@DataProvider(name="fetchData")
	public Object[][] getData()
	{
		return ReadExcel.readData(dataSheetName);
	}

}
