package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import utils.BasicReport;

public class SeleniumBase extends BasicReport implements Browser, Element{

	public RemoteWebDriver driver;
	public int i =1;
	Select selElement;
	String txt;
	
	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		
		try {
			
			if(browser.equalsIgnoreCase("chrome")) 
			{
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		    } 
			
			else if(browser.equalsIgnoreCase("firefox"))
			{
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver(); 
		    }
		    driver.manage().window().maximize();
		    driver.get(url);
		    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    logStep("The browser "+browser+" launched successfully","pass");
		    
            
		}
		catch(Exception e)
		{
			logStep("Browser not launched","fail");
			
		}
		
		finally 
		{
			takeSnap();
		}
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try
		{
		switch (locatorType) {
		case "id": return driver.findElementById(value);
		case "name": return driver.findElementByName(value);
		case "class": return driver.findElementByClassName(value);
		case "xpath": return driver.findElementByXPath(value);
		case "linktext": return driver.findElementByLinkText(value);
		case "partiallinktext": return driver.findElementByPartialLinkText(value);
		
		} // endOfSwitch
		}//endOfTryBlock
		catch(Exception e)
		{
			logStep("Inside Locate element method catch block \n Exception "+e.toString(),"fail");
			//System.out.println("Inside Locate element method catch block \n Exception "+e.toString());
			
		}
		
		
		finally 
		{
			takeSnap();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void switchToAlert() {
		try
		{
		driver.switchTo().alert();
		logStep("Switched to alert ","pass");
		//System.out.println("Switched to alert");
		}
		catch(Exception e)
		{
			logStep("Inside SwitchToAlert() method : catch block "+e.toString(),"Fail");
			//System.out.println("Inside SwitchToAlert() method : catch block "+e.toString());
		
		}
	}

	@Override
	public void acceptAlert() {
		try
		{
			driver.switchTo().alert().accept(); 
			logStep("Accepted the alert","pass");
			//System.out.println("Accepted the alert");

		}
		catch(Exception e)
		{
			logStep("Inside acceptAlert() method"+e.toString(),"fail");
			//System.out.println("Inside acceptAlert()"+e.toString());
		}
		
	}

	@Override
	public void dismissAlert() {
		
		try
		{
			driver.switchTo().alert().dismiss();
			logStep("Cancelled/Dismissed the alert","pass");
			//System.out.println("Cancelled/Dismissed the alert");
		}
		catch(Exception e)
		{
			logStep("Inside dismissAlert() method"+e.toString(),"fail");
			//System.out.println("Inside dismissAlert() method"+e.toString());
		}
		

	}

	@Override
	public String getAlertText() {
		String txtAlert="";
		try
		{
		 txtAlert=driver.switchTo().alert().getText();
		logStep("Inside getAlertText() method"+txtAlert,"pass");
		 //System.out.println("Inside getAlertText() method"+txtAlert);
		return txtAlert;
		}
		catch(Exception e)
		{
			logStep("Inside getAlertText() method"+e.toString(),"fail");
			//System.out.println("Inside getAlertText() method"+e.toString());
		}
		finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public void typeAlert(String data) {
		
		try {
		driver.switchTo().alert().sendKeys(data);
		logStep("Entered the value "+data+ " into alert window","pass");
		}
		catch(Exception e)
		{
			logStep("Inside typeAlert() method"+e.toString(),"fail");
		}
	}

	@Override
	public void switchToWindow(int index) {
		
		
		
	}

	@Override
	public void switchToWindow(String title)  {
		try
		{
		driver.switchTo().window(title);
		logStep("Switched to window :  "+title,"pass");
		
		}
		catch(Exception e)
		{
			logStep("Inside switchToWindow(String title) method"+e.toString(),"fail");
		}
		finally
		{
			takeSnap();
		}

	}

	@Override
	public void switchToFrame(int index) {
		try
		{
		driver.switchTo().frame(index);
		logStep("Switched to frame by using index  :"+index,"pass");
		
		}
		catch(Exception e)
		{
			logStep("Inside switchToFrame(int index) method"+e.toString(),"fail");
		}
		

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
		driver.switchTo().frame(ele);
		logStep("Switched to frame by using WebElement  :"+ele,"pass");
		takeSnap();
		
	}
	catch(Exception e)
	{
		logStep("Inside switchToFrameUsingWebElement method"+e.toString(),"fail");
	}
		finally 
		{
		takeSnap();
		}
	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
		driver.switchTo().frame(idOrName);
		logStep("Switched to frame by using idOrName  :"+idOrName,"pass");
		
		}
		catch(Exception e)
		{
			logStep("Inside switchToFrame(String idorName) method"+e.toString(),"fail");
		}
		finally 
		{
		takeSnap();
		}


	}

	@Override
	public void defaultContent() {
		try {
		driver.switchTo().defaultContent();
		logStep("Switched to Main DOM/Page","pass");
        takeSnap();
		}
		catch(Exception e)
		{
			logStep("Inside defaultContent() method"+e.toString(),"fail");
		}
		finally 
		{
		takeSnap();
		}
	}

	@Override
	public boolean verifyUrl(String url) {
		try {
		String getUrl= driver.getCurrentUrl();
		if(getUrl.contains(url))
		{
			logStep("Current URL :"+getUrl,"pass");
			
			return true;
		}
		
		
		}
		catch(Exception e)
		{
			logStep("Inside verifyUrl(String url) method"+e.toString(),"fail");
		}
		finally 
		{
		takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		try {
		String getTitel=driver.getTitle();
		logStep("Current Titel"+getTitel,"pass");
		//System.out.println("Current Titel"+getTitel);
		takeSnap();
		if(getTitel.contains(title))
		return true;
		
		}
		
		catch(Exception e)
		{
			logStep("Inside verifyTitle(String url) method"+e.toString(),"fail");
			//System.out.println("Inside verifyTitle(String url) method"+e.toString());
		}
		finally 
		{
		takeSnap();
		}
		return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
			logStep("Copying file from "+src+" to destination file location :"+des,"pass");
			//System.out.println("Copying file from "+src+" to destination file location :"+des);
		} catch (IOException e) {
			
			logStep("Inside takeSnap() method","fail");
			//System.out.println("Inside takeSnap() method");
			e.printStackTrace();
		}
		
		i++;
	}

	@Override
	public void close() {
		
		//System.out.println("Closing the current tab of browser");
		//takeSnap();
		driver.close();

	}

	@Override
	public void quit() {
		System.out.println("Closing the all the  tabs of browser");
		//takeSnap();
		driver.quit();
		

	}
	@Override
	public void clickWithoutSnap(WebElement ele)
	{
		try
		{
			ele.click();
			logStep("The element "+ele+" clicked successfully","pass");
			//System.out.println("The element "+ele+" clicked successfully");
			}
		
		catch(Exception e)
		{
			logStep("Inside clickWithoutSnap() method"+e.toString(),"fail");
			//System.out.println("Inside clickWithoutSnap() method"+e.toString());
		}
		finally 
		{
		takeSnap();
		}
		
		
		
	}
	
	

	@Override
	public void click(WebElement ele) {
		try {
		ele.click();
		logStep("The element "+ele+" clicked successfully","pass");
		//System.out.println("Clicked on element : "+ele);
		
	}
	catch(Exception e)
	{
		logStep("Inside click() method"+e.toString(),"fail");
		//System.out.println("Inside click() method"+e.toString());
	}
		finally 
		{
		takeSnap();
		}
	
		
	}

	@Override
	public void append(WebElement ele, String data) {
		
		try {
		String getText = ele.getText();
		
		logStep(" Appended the value "+data+ " with "+getText,"pass");
		ele.sendKeys(getText+" "+data);
		takeSnap();
		}
		catch(Exception e)
		{
			logStep("Inside append(ele,data) method"+e.toString(),"fail");
		}
		finally 
		{
		takeSnap();
		}
	}

	@Override
	public void clear(WebElement ele) {
		 
		try
		{
		takeSnap();
		logStep("Taken Screenshot before clearing the webelement","pass");
		ele.clear();
		takeSnap();
		logStep("Taken Screenshot after clearing the webelement","pass");
		}
		
		catch(Exception e)
		{
			logStep("Inside clear() method"+e.toString(),"fail");
			
		}
		finally 
		{
		takeSnap();
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try
		{
		ele.clear();
		ele.sendKeys(data); 
		logStep("The data "+data+" entered successfully","pass");
		//System.out.println("The data "+data+" entered successfully");
		}
		catch(Exception e)
		{
			logStep("Inside clearAndType() method"+e.toString(),"fail");
			//System.out.println("Inside clearAndType() method"+e.toString());
			
		}
		finally 
		{
		takeSnap();
		}
		
	}
	@Override
	public String getElementText(WebElement ele) {
		String txt="";
		try {
		 txt = ele.getText();
		logStep("Inside getText() method"+txt,"pass");
		 //System.out.println("Inside getText() method"+txt);
		return (txt); 
		}
		catch(Exception e)
		{
			logStep("Inside getElementText() method"+e.toString(),"fail");
			//System.out.println("Inside getElementText() method"+e.toString());
			
		}
		finally {
			takeSnap();
			
					
		}
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		String backgroundColor ="";
		try {
		 backgroundColor=ele.getAttribute(getBackgroundColor(ele));
		
		logStep("Backgroung color of an webelement "+backgroundColor,"pass");
		return backgroundColor;
		}
		catch(Exception e)
		{
			logStep("Inside getBackgroundColor() method"+e.toString(),"fail");
			
			
		}
		finally {
			takeSnap();
			
		}
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		String txt="";
		try
		{
			txt=ele.getText();
		
		logStep("Value of typed text "+ele.getText(),"pass");
			//System.out.println("Value of typed text "+ele.getText());
		return txt;
		}
		catch(Exception e)
		{
			logStep("Inside getTypedText() method"+e.toString(),"fail");
			//System.out.println("Inside getTypedText() method"+e.toString());
			
			
		}
		finally {
			takeSnap();
			
		}
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
		selElement = new Select(ele);
		selElement.selectByVisibleText(value);
		
		logStep("Selected the value by using select by value "+value,"pass");
		//System.out.println("Selected the value by using select by value "+value);
		}
		catch(Exception e)
		{
			logStep("Inside selectDropDownUsingText() method"+e.toString(),"fail");
			//System.out.println("Inside selectDropDownUsingText() method "+e.toString());
			
		}
		finally {
			takeSnap();
			
		}

		
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
		selElement =new Select(ele);
		selElement.selectByIndex(index);
		
		logStep("Selected index by using selectByIndex method "+index,"pass");
		
		}
		catch(Exception e)
		{
			logStep("Inside selectDropDownUsingIndex() method"+e.toString(),"fail");

			
		}
		finally {
			takeSnap();
			
		}

		
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
		selElement =new Select(ele);
		selElement.selectByValue(value);
		logStep("Selected value by using selectByVisibleText() method "+value,"pass");
		//takeSnap();
        //System.out.println("Selected value by using selectByVisibleText() method "+value);
		}
		
		catch(Exception e)
		{
			logStep("Inside selectDropDownUsingValue() method"+e.toString(),"fail");
			//System.out.println("Inside selectDropDownUsingValue() method"+e.toString());
			
		}
		finally {
			takeSnap();
			
		}

		
		
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		
		String txtFromWebElement;
		try {
		txtFromWebElement = ele.getText();
		if(txtFromWebElement.equals(expectedText))
		{
			logStep("Given text "+expectedText+" is matching with txt from webelement "+txtFromWebElement,"pass");
			//System.out.println("Given text \"+expectedText+\" is matching with txt from webelement "+txtFromWebElement);
			
			return true;
			
		}
		else
		{
			logStep("Given text "+expectedText+" is not matching with txt from webelement "+txtFromWebElement,"pass");
			//System.out.println("Given text \"+expectedText+\" is not matching with txt from webelement "+txtFromWebElement);
			takeSnap();
			return false;
			
		}
		}
		catch(Exception e)
		{
			logStep("Inside verifyExactText() method"+e.toString(),"fail");
			//System.out.println("Inside verifyExactText() method"+e.toString());
			takeSnap();
			
			
		}
		finally
		{
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		
		String txtFromWebElement;
		try {
		 txtFromWebElement= ele.getText();
		if(txtFromWebElement.contains(expectedText))
		{
			logStep("Given text "+expectedText+" contains the  txt from webelement ","pass");
			//System.out.println("Given text "+expectedText+" contains the  txt from webelement ");
			takeSnap();
			return true;
			
		}
		else
		{
			logStep("Given text "+expectedText+" doesnt contain  txt from webelement "+txtFromWebElement,"pass");
			//System.out.println("Given text "+expectedText+" doesnt contain  txt from webelement "+txtFromWebElement);
			takeSnap();
			return false;
		}
		}
		catch(Exception e)
		{
			logStep("Inside verifyPartialText() method"+e.toString(),"fail");
			//System.out.println("Inside verifyPartialText() method"+e.toString());
			
			
			
		}
		finally
		{
			takeSnap();
		}
		return false;
	
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		String txtFromWebElement;
		try {
		 txtFromWebElement = ele.getAttribute(value);
		if(txtFromWebElement.equals(value))
		{
			logStep("Given text "+value+" is matching with txt from webelement "+txtFromWebElement,"pass");
			//System.out.println("Given text "+value+" is matching with txt from webelement "+txtFromWebElement);
			takeSnap();
			return true;
			
		}
		else
		{
			logStep("Given text "+value+" is not matching with txt from webelement "+txtFromWebElement,"pass");
			//System.out.println("Given text "+value+" is not matching with txt from webelement "+txtFromWebElement);
			takeSnap();
			return false;
		}
	}catch(Exception e)
	{
		logStep("Inside verifyExactAttribute() method"+e.toString(),"fail");
		//System.out.println("\"Inside verifyExactAttribute() method\"+e.toString()");
		return false;
		
	}
	finally
	{
		takeSnap();
	}
		
	
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		try
		{
		boolean displayed = ele.isDisplayed();
		
		
		if(displayed) 
			{
			logStep("Given ele "+ele+" is Displayed","pass");
			return true;
			}
		else
		{
			logStep("Given ele "+ele+" is not Displayed","pass");	
		return false;
		}
	}
		catch(Exception e)
		{
			logStep("Inside verifyDisplayed() method"+e.toString(),"fail");
			takeSnap();
			return false;
			
		}
		finally
		{
			takeSnap();
		}
			
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		
		try {
			boolean enabled = ele.isEnabled();
		
		
		
		if(enabled) 
			{
			logStep("Given ele "+ele+" is Enabled","pass");
			return true;
			}
		else
		{
			logStep("Given ele "+ele+" is not Enabled","pass");	
		return false;
		}
	}
	catch(Exception e)
	{
		logStep("Inside verifyEnabled() method"+e.toString(),"fail");
		
		return false;
		
	}
	finally
	{
		takeSnap();
	}
		
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		try {
		boolean selected = ele.isSelected();
		
		
		if(selected) 
			{
			logStep("Given ele "+ele+" is selected","pass");
			return true;
			}
		else
		{
			logStep("Given ele "+ele+" is not selected","pass");	
		return false;
		}
		}
		catch(Exception e)
		{
			logStep("Inside verifySelected() method"+e.toString(),"fail");
			//takeSnap();
			return false;
			
		}
		finally
		{
			takeSnap();
		}
			
	}

}
