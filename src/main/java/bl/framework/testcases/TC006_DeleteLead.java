//test
package bl.framework.testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import bl.framework.api.SeleniumBase;

public class TC006_DeleteLead extends SeleniumBase {
	
	
	public void deleteLead() throws InterruptedException
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		
		//click on crm/sfa link
		
		WebElement eleCRMSFALink = locateElement("xpath", "//a[contains(text(), 'CRM/SFA')]");
		click(eleCRMSFALink);
		
		//click on leads in menu bar
		WebElement eleLeadsLink = locateElement("xpath", "//a[text()='Leads']");
		click(eleLeadsLink);
		
		//Find Leads
		//click on Find leads in menu bar
				WebElement eleFindLeadsLink = locateElement("xpath", "//a[text()='Find Leads']");
				click(eleFindLeadsLink);
		//click on email tab
				
				
				WebElement eleEmailTab = locateElement("xpath", "//span[text()='Email']");
				click(eleEmailTab);
				
				//Enter email id
				WebElement eleEmailTxt = locateElement("xpath", "//input[@name='emailAddress']");
				clearAndType(eleEmailTxt, "Test121@gmail.com");
				
		//Click on find leads button -
				
				WebElement eleFindLeadsBtn = locateElement("xpath", "//button[text()='Find Leads']");
				click(eleFindLeadsBtn);
				
				//select first matching record
				//identify table - 
				
				WebElement eleTableName = locateElement("xpath", "(//table[@class='x-grid3-row-table'])[1]");
				  List<WebElement> rows = eleTableName.findElements(By.tagName("tr"));
				 WebElement firstrow = rows.get(0);
			 List<WebElement> columns = firstrow.findElements(By.tagName("td"));
				 String firstMatchingLeadid=columns.get(0).getText();
				 
				 WebElement eleFirstMatchingLeadId = locateElement("linktext",firstMatchingLeadid );
				 click(eleFirstMatchingLeadId);
				 
				 //click on Delete button
				 
				 ////a[text()='Delete']
				 WebElement eleDeleteBtn = locateElement("linktext", "Delete");
					click(eleDeleteBtn);
					
		
	}

}
