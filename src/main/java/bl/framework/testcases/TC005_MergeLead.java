package bl.framework.testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods {
	
	@BeforeTest
	public void setData(String testcaseName, String testDec, String author, String category) {
		
		   testcaseName = "TC004_MergeLead";
		   testDec ="Merge Lead Test case description";
		   author= "Nishanthi";
		   category ="Smoke";
		   
		   initializeTest(testcaseName, testDec, author, category);
	} 
	//@Test(groups="sanity")
	public void mergeLead() throws InterruptedException
	{
		/*
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); */
		//
		
		//Login
		//login();
		
		//click on crm/sfa link
		
		WebElement eleCRMSFALink = locateElement("xpath", "//a[contains(text(), 'CRM/SFA')]");
		click(eleCRMSFALink);
		
		//click on leads in menu bar
		WebElement eleLeadsLink = locateElement("xpath", "//a[text()='Leads']");
		click(eleLeadsLink);
		
		//Click on merge lead link
		WebElement eleMergeLeadsLink = locateElement("partiallinktext", "Merge Leads");
		click(eleMergeLeadsLink);
		
		//Click on lookup icon
				WebElement eleLookupIcon = locateElement("xpath", "//img[@alt='Lookup']");
				click(eleLookupIcon);
				
				//Switch to Window - invoke switchToWindow() method
				
				Set<String> windowHandles = driver.getWindowHandles();
				List<String> ls=new ArrayList<String>();
				ls.addAll(windowHandles);
				switchToWindow(ls.get(1));
				
				Thread.sleep(2000);
				//maximize new window
				
				//driver.manage().window().maximize();
				
				//Enter from lead id
				
				WebElement eleFrmLeadId = locateElement("name", "id");
				clearAndType(eleFrmLeadId, "10464"); 
				
				//-----------------------------------------
				
				//Click find leads button
				
				WebElement eleFindLeadBtn = locateElement("xpath", "//button[text()='Find Leads']");
				click(eleFindLeadBtn);
				Thread.sleep(3000);
				
				//select the first row lead id
				
				
				WebElement table = driver.findElementByXPath("//table[@class='x-grid3-row-table']");
				 List<WebElement> rows = table.findElements(By.tagName("tr"));
				WebElement firstrow = rows.get(0);
				
				List<WebElement> column = firstrow.findElements(By.tagName("td"));
				String leadid = column.get(0).getText();
				
				WebElement eleLeadIDCell = locateElement("partiallinktext", leadid);
				clickWithoutSnap(eleLeadIDCell);
				
				
				
				
				
				
				
				//Switch back to primary window
				switchToWindow(ls.get(0));
				Thread.sleep(1000);
				
				//click on merge button
				//driver.findElementByXPath("//a[text()='Merge']").click();
				
				//handle alert
				
				//driver.switchTo().alert().accept();
				
				//click search icon next to to lead textbox
				
				WebElement eleToLookupIcon = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
				clickWithoutSnap(eleToLookupIcon);
				
				
				//Switch the control to new window
				Set<String> windowHandles1 = driver.getWindowHandles();
					 ls=new ArrayList<String>();
						ls.addAll(windowHandles1);
						System.out.println(ls.size());
						switchToWindow(ls.get(1));
						
						
						Thread.sleep(2000);
						//maximize new window
						
						//driver.manage().window().maximize();
						
						//Enter lead id
						
						//Enter to lead id
						
						WebElement eleToLeadId = locateElement("name", "id");
						clearAndType(eleToLeadId, "10463"); 
						
						
						
						
						
						//Click find leads button
						 eleFindLeadBtn = locateElement("xpath", "//button[text()='Find Leads']");
						click(eleFindLeadBtn);
						Thread.sleep(3000);
						
						//select the first row lead id
						
						
						 table = driver.findElementByXPath("//table[@class='x-grid3-row-table']");
						 rows = table.findElements(By.tagName("tr"));
						 firstrow = rows.get(0);
						
						 column = firstrow.findElements(By.tagName("td"));
						String toleadid = column.get(0).getText();
						
						 WebElement eleToLeadIDCell = locateElement("partiallinktext", toleadid);
							clickWithoutSnap(eleToLeadIDCell);
						
						//Switch back to primary window
						
						switchToWindow(ls.get(0));
						Thread.sleep(1000);
				
						//click on merge button
						 WebElement eleMergeLeadBtn = locateElement("xpath", "//a[@class='buttonDangerous']");
						 clickWithoutSnap(eleMergeLeadBtn);
						 //click(eleMergeLeadBtn);
						
						//switch to alert and accept
						acceptAlert();
						
						Thread.sleep(1000);
						//click on find leads link
						
						 WebElement eleFindLeadLink = locateElement("xpath", "//a[text()='Find Leads']");
						click(eleFindLeadLink);
						
						Thread.sleep(1000);
						//enter value in lead id text box
						WebElement eleLeadIdTxt = locateElement("name", "id");
						clearAndType(eleLeadIdTxt,"10464");
						
						
						
						Thread.sleep(1000);
						//click on find leads button
						 eleFindLeadBtn = locateElement("xpath", "(//td[@class='x-panel-btn-td'])[6]");
						click(eleFindLeadBtn);
						
						//Verify that , there is no record displayed in table
						
						//to close the browser
						 close();
						
				
				
				
				
		
	}

}
