package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	static Object[][] data;
	public static Object[][] readData(String dataSheetName)
	{
		
		try {
			//Entering into workbook
			//Mention filename
			XSSFWorkbook wbook = new XSSFWorkbook("./Data/"+dataSheetName+".xlsx");
			
			
			//entering into sheet
			
			XSSFSheet sheet = wbook.getSheet("Sheet1");
			
			//Get row count
			
			int rowCount = sheet.getLastRowNum();  
			// getLastRowNum () - apache always ignore header count- so it will return remaining row count
			System.out.println("Row count : "+rowCount);
			//Get column count
			
		    int colCount = sheet.getRow(0).getLastCellNum();
		    System.out.println("Column count : "+colCount);
		    
		    data=new Object[rowCount][colCount];
		    
		    //Get all row and cell values 
		    //we use for loop
		    
		    //to ignore header count , start row index from 1 
		    for(int i=1;i<=rowCount;i++)
		    {
		    	
		    	//Entering into row
		    	XSSFRow row = sheet.getRow(i);
		    	
		    	//to read cell values 
		    	System.out.println("Reading values from row : "+i);
		    	for (int j = 0; j < colCount; j++) {
		    		
		    		XSSFCell cell = row.getCell(j);
		    		
		    		
		    		//To get the cell data
		    		
		    		//String text = cell.getStringCellValue();
		    		data[i-1][j]=cell.getStringCellValue();
		    		
					
				}    	
		    	
		    }

			
			
			
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
		
		return data;
	}

}
