package facebook;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class TC001_Search_TestLeaf {
	
@Test
public void Search_Test_Leaf() throws InterruptedException
{
	//to turn off notifications
	ChromeOptions options = new ChromeOptions();
	options.addArguments("--disable-notifications");

	
	//launch the browser
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver(options);
			
			//To navigate to url
			driver.get("https://www.facebook.com/");
			
			//to maximize
			driver.manage().window().maximize();
			
			//implicit
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			//enter username
		WebElement txt_Email = driver.findElementByName("email");
		txt_Email.clear();
			txt_Email.sendKeys("nishanthimayaa@gmail.com");
			
			//enter password
			driver.findElementById("pass").sendKeys("shanthi1234");
			
			//click on login
			driver.findElementById("loginbutton").click();
			
			 
				
			
			//locate search textbox - enter test leaf in search textbox
			
			driver.findElementByXPath("//input[@class='_1frb']").sendKeys("testleaf");
			
			//click search icon
			driver.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
			
			
			//Get the text button of like
			WebElement eleGetTextOfLikeBtn = driver.findElementByXPath("(//button[@type='submit'])[2]");
			String txtLikeBtn = eleGetTextOfLikeBtn.getText();
			
			System.out.println("Text of like button "+ txtLikeBtn);
			
			
			////div[@class='_52eh _5bcu']
			/*
			//verify the test leaf is present in searchresults
			List<WebElement>  Search_Results = driver.findElementsByXPath("//div[@class='_52eh _5bcu']");
			
			String str_Search_Results;
			
			for(int i=0;i<Search_Results.size();i++)
			{
				str_Search_Results =Search_Results.get(i).getText();
				
				if(str_Search_Results.equalsIgnoreCase("TestLeaf"))
				{
					System.out.println("Test leaf is present in search resutls");
					Search_Results.get(i).click();
				}
				
				else
					System.out.println("Test leaf not present in search results");
			} */
			
			Thread.sleep(1000);
			
			
			
			//get the text of like button in test leaf page
			
			//button[@class='likeButton _4jy0 _4jy4 _517h _51sy _42ft']
			//WebElement Btn_Like_Ele = driver.findElementByXPath("(//button[@type='submit'])[2]");
			//String txt_Like_Get_Text = Btn_Like_Ele.getText();
			
			//System.out.println(txt_Like_Get_Text);
			
			
			if(txtLikeBtn.equalsIgnoreCase("Like"))
			{
				System.out.println("found the like button in test leaf page");
				
				eleGetTextOfLikeBtn.click();
			}
			else 
			{
				//WebElement Link_Liked_Ele = driver.findElementByXPath("//button[text()='Share']/preceding::span[3]/preceding::span[2]");
				//String txt = Link_Liked_Ele.getText();
				
				if(txtLikeBtn.equalsIgnoreCase("liked"))
				{
					System.out.println("Page is already liked");
				}
			} 
			
			//Click of testleaf page
			
			driver.findElementByXPath("//div[text()='TestLeaf']").click();
			
			
			//Verify testleaf page title
			
			
			WebElement ele_TestLeaf_Page_Title = driver.findElementByXPath("//a[@class='_64-f']");
			
			String TestLeaf_Page_Title = ele_TestLeaf_Page_Title.getText();
			
			if(TestLeaf_Page_Title.equalsIgnoreCase("TestLeaf"))
			{
				System.out.println("Title contains TestLeaf");
			}
			else
				System.out.println("Title not contains test leaf");
			
			
			//to get the number of likes for the page
			
			
			WebElement ele_Num_of_Likes = driver.findElementByXPath("//div[@class='_4-u2 _6590 _3xaf _4-u8']/div[3]/div/div[2]/div");
			
			String Num_Of_Likes =ele_Num_of_Likes.getText();
			
			System.out.println(" Num of likes :: "+Num_Of_Likes); 
}
}