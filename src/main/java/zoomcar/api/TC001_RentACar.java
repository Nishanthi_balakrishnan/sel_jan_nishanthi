package zoomcar.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC001_RentACar {
	
	@Test
	public void rentACar()
	{
		//launch the browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		//To navigate to url
		driver.get("https://www.zoomcar.com/chennai");
		
		//to maximize
		driver.manage().window().maximize();
		
		//implicit
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		
		
		//to click on Start beautiful journey
		
		driver.findElementByClassName("search").click();
		
		
		//to click on popular pick up point
		
		driver.findElementByXPath("//div[@class='component-popular-locations']/div[2]").click();
		
		//to click on next after selecting the popular pick up point
		
		driver.findElementByXPath("//button[text()='Next']").click();
		
		//to select journey date
		
		// Get the current date
		
		Date date = new Date();

		// Get only the date (and not month, year, time etc)
				
		DateFormat sdf = new SimpleDateFormat("dd");
		 
		// Get today's date
				
		String today = sdf.format(date);

		// Convert to integer and add 1 to it
		 
		int tomorrow = Integer.parseInt(today)+1;
		
		System.out.println("Tomorrow's date : "+tomorrow);
		
		//to select tomorrow's date
		driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").click();
		
	// to click next
		driver.findElementByXPath("//button[text()='Next']").click();
		
		//to click done
		driver.findElementByXPath("//button[contains(text(),'Done')]").click();
		
		//to get the search results
		
		//div[@class='car-list-layout']/div
		
		//WebElement Search_Results_Car = driver.findElementByXPath("//div[@class='car-list-layout']/div");
		
		//to print the count of search results
		
		List<WebElement> List_of_Cars = driver.findElementsByXPath("//div[@class='price']");
		
		System.out.println("Num of cars in list :"+(List_of_Cars.size()-1));
		
		//to compare the price list
		
		List<Integer> Car_Price_List= new ArrayList<Integer>();
		
		String price;
		for(int i =0;i<List_of_Cars.size();i++)
		{
			price= List_of_Cars.get(i).getText();
			System.out.println(price.replaceAll("[^0-9]", ""));
			
			
			Car_Price_List.add(Integer.parseInt(price.replaceAll("[^0-9]", "")));
			
			//price.replaceAll("[^a-zA-Z0-9]", "");
					
		}
		
		
		Collections.sort(Car_Price_List);
		
		//Get the maximum car price
		int Max_Car_Price=Car_Price_List.get(Car_Price_List.size()-1);
		System.out.println("Maximum car rental price :"+Max_Car_Price);
		
		//div[contains(text(),'691')]
		
		WebElement ele_Car_Brand_Name = driver.findElementByXPath("//div[contains(text(),'"+Max_Car_Price+"')]/../../preceding-sibling::div[1]/h3");
		String Car_Brand_Name=ele_Car_Brand_Name.getText();
		System.out.println("Car brand name : "+Car_Brand_Name);
		
		////div[@class='price']/following-sibling::button[@name='book-now']
		
		//Click on book now of car-which has max price
		
		driver.findElementByXPath("//div[@class='price']/following-sibling::button[@name='book-now']").click();
		System.out.println("Clicked on book now button");
		
		
		//to close the browser
		//driver.close();
		
		
	}

}
